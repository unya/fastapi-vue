import 'bootstrap/dist/css/bootstrap.css';
import Vue from 'vue';

import App from './App.vue';
import router from './router';
import store from './store';

import axios from 'axios' ;

axios.defaults.withCredentials = true;
axios.defaults.baseURL = '/api';  // the FastAPI backend
//axios.defaults.baseURL = 'http://localhost:8080/api';  // the FastAPI backend

Vue.config.productionTip = false;

//
// https://axios-http.com/docs/interceptors
//
axios.interceptors.request.use(request => {
    console.log('Request: ', request)
    return request
}, function (error) {
    // Do something with request error
    return Promise.reject(error);
});

axios.interceptors.response.use(function (response) {
    console.log('Response: ', response) ;
    return response;
  },

  function (error) {
    if (error) {
      const originalRequest = error.config;
      if (error.response.status === 401 && !originalRequest._retry) {
        originalRequest._retry = true;
        store.dispatch('logOut');
        return router.push('/login')
      }
    }
  });

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app');
