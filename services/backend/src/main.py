from fastapi import FastAPI
from starlette.middleware.cors import CORSMiddleware

from tortoise import Tortoise

from src.database.register import register_tortoise
from src.database.config import TORTOISE_ORM


# enable schemas to read relationship between models
Tortoise.init_models(["src.database.models"], "models")

"""
import 'from src.routes import users, notes' must be after 'Tortoise.init_models'
why?
https://stackoverflow.com/questions/65531387/tortoise-orm-for-python-no-returns-relations-of-entities-pyndantic-fastapi
"""
from src.routes import users, notes

app = FastAPI()


#allow_origins=["*"]
allow_origins=["http://localhost",
               "http://localhost:8080",
               "http://127.0.0.1",
               "http://127.0.0.1:8080",
               ]


app.add_middleware(
    CORSMiddleware,
    allow_origins = allow_origins,
    allow_credentials = True,
    allow_methods = ["*"], #["GET", "POST", "OPTION", "HEAD"],
    allow_headers = ["*"],
)

app.include_router(users.router)
app.include_router(notes.router)

register_tortoise(app, config=TORTOISE_ORM, generate_schemas=True)


@app.get("/")
def home():
    return "Hello, World!"
