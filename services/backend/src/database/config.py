import os


TORTOISE_ORM = {
    "connections": { "default": os.environ.get("DATABASE_URL") or "sqlite://db.sqlite" },
    "apps": {
        "models": {
            "models": [
                "src.database.models", "aerich.models"
            ],
            "default_connection": "default"
        }
    }
}
